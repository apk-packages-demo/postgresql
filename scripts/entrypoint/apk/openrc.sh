. $CI_PROJECT_DIR/scripts/entrypoint/sh/header.sh

if ! id -u user &> /dev/null ; then
    apk add \
            --update-cache \
            --quiet \
        catatonit \
        openrc \
        setpriv \
        sudo \
    ;
    adduser -D user
    echo "user ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/user
    fi
exec catatonit -- sh $CI_PROJECT_DIR/scripts/catatonit-task/sh/openrc-sudo.sh "$@"



