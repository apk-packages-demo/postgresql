# postgresql
## Official documentation
### Use of syslog
If no syslog deamon is present postgres will output messages on the console.
* [*Error Reporting and Logging*
  ](https://www.postgresql.org/docs/11/runtime-config-logging.html)